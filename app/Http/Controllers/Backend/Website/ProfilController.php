<?php

namespace App\Http\Controllers\Backend\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Helpers\Guzzle;

/**
 * Class HomeController.
 */
class ProfilController extends Controller
{
    public function index()
    {
        $response = $this->guzzle->get([],env('API_URL'),'website/profil/read');
        $result = $this->guzzle->getContents($response);

        $data = [
            'title' => 'Panel User',
            'data' => $result
        ];
        return view('backend.website.profil', $data);
    }

    public function get()
    {
        $response = $this->guzzle->get([],env('API_URL'),'website/profil/get');
        $result = $this->guzzle->getContents($response);

        if ($response->getStatusCode() == '200') {
            $data= [
                'data' => $result->data->data
            ];

            return view('backend.table.user', $data);
        }

        return view('backend.table.user')->with('error', $result->error->message);
    }

    public function create(Request $request)
    {
        $response = $this->guzzle->post($request->all(),env('API_URL'),'user/create');
        $result = $this->guzzle->getContents($response);

        $data = [
            'data' => $result
        ];

        return $data;
    }

    public function formCreate()
    {
        $reqroles = $this->guzzle->get([],env('API_URL'),'user/role/get');
        $roles = $this->guzzle->getContents($reqroles);

        $reqpermission = $this->guzzle->get([],env('API_URL'),'user/permission/get');
        $permission = $this->guzzle->getContents($reqpermission);

        return view('backend.form.user', ['roles' => $roles, 'permission' => $permission]);
    }

    public function update(Request $request)
    {
        $response = $this->guzzle->post($request->all(),env('API_URL'),'website/profil/update/1');
        $result = $this->guzzle->getContents($response);

        $data = [
            'data' => $result
        ];

        return $data;
    }

    public function formEdit(Request $request)
    {
        $reqroles = $this->guzzle->get([],env('API_URL'),'user/role/get');
        $roles = $this->guzzle->getContents($reqroles);

        $reqpermission = $this->guzzle->get([],env('API_URL'),'user/permission/get');
        $permission = $this->guzzle->getContents($reqpermission);

        $reqdata = $this->guzzle->get(['id' => $request->id],env('API_URL'),'user/get-id');
        $data = $this->guzzle->getContents($reqdata);

        return view('backend.form.edituser', ['roles' => $roles, 'permission' => $permission, 'data' => $data->data]);
    }

    public function delete(Request $request)
    {
        $response = $this->guzzle->post($request->all(),env('API_URL'),'user/delete');
        $result = $this->guzzle->getContents($response);

        $data = [
            'data' => $result
        ];

        return $data;
    }
}
