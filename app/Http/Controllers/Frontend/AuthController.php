<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Helpers\Guzzle;

/**
 * Class HomeController.
 */
class AuthController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function loginPage()
    {
    	$data = [
    		'title' => 'Login'
    	];
        return view('auth.login', $data);
    }

    public function loginApi(Request $request)
    {
        $parameter = [
            'email' => $request->email,
            'password' => $request->password
        ];

        $response = $this->guzzle->post($parameter,env('API_URL'),'auth/login');
        $result = $this->guzzle->getContents($response);
 
        if($response->getStatusCode() == 200){

            if ($result->permission != null) {
                foreach ($result->permission as $key) {
                    $data_permission[$key->name] = $key->name;
                }
            }

            $request->session()->put('token_user', $result->token);
            $request->session()->put('user', $result->data);
            $request->session()->put('role', $result->role);
            $request->session()->put('permission', $data_permission);
        }

        $data = [
            'data' => $result
        ];
        return $data;
    }

    public function registerPage()
    {
        $data = [
            'title' => 'Register'
        ];
        return view('auth.register', $data);
    }

    public function registerApi(Request $request)
    {
        $parameter = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'callback_url' => $request->callback_url,
        ];

        $response = $this->guzzle->post($parameter,env('API_URL'),'auth/signup');
        $result = $this->guzzle->getContents($response);

        $userrole = $this->guzzle->post(['email' => $request->email, 'role_name' => 'user'],env('API_URL'),'user/role/user-create');

        $data = [
            'data' => $result
        ];
        return $data;
    }

    public function confirmation(Request $request)
    {
        $response = $this->guzzle->post($request->all(),env('API_URL'),'auth/confirmation');
        $result = $this->guzzle->getContents($response);

        return redirect('login-page')->with('confirmed', 'Your Account is confirmed! Login to enjoy our journey');
    }

    public function logout(Request $request)
    {
        $parameter = [
            'token' => $request->session()->get('token_user'),
        ];

        $response = $this->guzzle->sendToken($parameter,env('API_URL'),'auth/logout');
        $result = $this->guzzle->getContents($response);
   
        return redirect()->back();
    }
}
