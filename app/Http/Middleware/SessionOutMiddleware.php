<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Helpers\Guzzle;

class SessionOutMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('user')){
            // check if in API still login or not
            $response = Guzzle::sendToken(['token' => $request->session()->get('token_user')], env('API_URL'), 'check-token');
            
            if($response->getStatusCode() != 200){
                $request->session()->forget('token_user');
                $request->session()->forget('user');
                $request->session()->forget('permission');
                $request->session()->forget('user_role');

                return $next($request);
            }

            return $next($request);
        }else{
            return $next($request);
        }
    }
}