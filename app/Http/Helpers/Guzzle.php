<?php 

namespace App\Http\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class Guzzle
{
	public static function post($parameter = [], $base_url = '', $url = '')
	{
		$parameter =  array_merge($parameter, [
			'token' =>env('API_KEY')
		]);	

		$auth = [
			'form_params' => $parameter
		];

		try {
			$client = new \GuzzleHttp\Client(['base_uri' => $base_url]);
			$response = $client->request('POST', $url, $auth);
		} catch (RequestException $exception) {
			$response = $exception->getResponse();
		}

		if(!is_null($response)){
			return $response;
		} else return redirect()->route('error.apidown');
	}

	public static function get($parameter, $base_url, $url)
	{
		$param = http_build_query($parameter); 
		$parameter = $url."?".$param;

		$auth = [
			'headers' => [
				'Authorization' => 'Bearer '.env('API_KEY')
			]
		];

		try {
	     	$client = new \GuzzleHttp\Client(['base_uri' => $base_url]);
	     	$response = $client->request('GET', $parameter, $auth);
		} catch (RequestException $exception) {
			$response = $exception->getResponse();
		}
		
		if(!is_null($response)){
			return $response;
		} else return redirect()->route('error.apidown');
	}

	public static function sendToken($parameter = [], $base_url = '', $url = '')
	{
		$auth = [
			'form_params' => $parameter
		];
		
		try {
			$client = new \GuzzleHttp\Client(['base_uri' => $base_url]);
			$response = $client->request('POST', $url, $auth);
		} catch (RequestException $exception) {
			$response = $exception->getResponse();
		}
		
		if(!is_null($response)){
			return $response;
		} else return redirect()->route('error.apidown');
	}

	public static function getContents($response)
	{
		$data = $response->getBody()->getContents();
		return json_decode($data);
	}

	/**
	* Get parameter API Value for internal
	* param array('value', 'condition')
	* recieve condition =, !=, <=, >=, like
	* not acceptable  for beetwen
	* return string
	*/
	public static function getParamValue($parameter)
	{
		return implode(',', $parameter);
	}
}