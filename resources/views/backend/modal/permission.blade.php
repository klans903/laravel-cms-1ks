<div class="modal fade" id="modalCreatePermission" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body load-permission" style="display: block">
                <div class="row">
                <div class="col-md-12 text-center">
                    <img src="{{ url('image/load.gif') }}" height="150px">
                    <p>Loading Data, Please wait...</p>
                </div>
            </div>
            </div>
                    
            <div class="form-permission">

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalDeletePermission" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Permission</h4>
            </div>
            <div class="modal-body load-permission" style="display: none">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="{{ url('image/load.gif') }}" height="150px">
                        <p>Loading Data, Please wait...</p>
                    </div>
                </div>
            </div>
            <div class="form-permission">
                <div class="modal-body">
                    <h4>Are you sure to delete this permission?</h4>
                </div> 
                <div class="modal-footer ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="delete-permission" class="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>
