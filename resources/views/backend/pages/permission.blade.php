@extends('backend.layouts.app')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{{ $title }}</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li class="active">{{ $title }}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- ============================================================== -->
        <!-- Sales different chart widgets -->
        <!-- ============================================================== -->
        <!-- .row -->
        <div class="row">
            <div class="col-md-8 col-lg-8">
                <div class="white-box">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 class="box-title m-l-10 m-t-5">ROLE</h3>
                            </div>
                            <div class="col-md-4 text-right">
                                <a data-toggle="modal" data-target="#modalCreateRole" class="formButtonRole"><button type="button" class="btn btn-info btn-rounded m-r-10"><i class="ti-plus"></i></button></a>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12">
                            <div class="text-center" id="load-role">
                                <img src="{{ url('image/load.gif') }}" height="200px">
                            </div>
                            <div id="tabel-role">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="white-box">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="box-title m-l-10 m-t-5">PERMISSION</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <a data-toggle="modal" data-target="#modalCreatePermission" class="formButtonPermission"><button type="button" class="btn btn-info btn-rounded m-r-10"><i class="ti-plus"></i></button></a>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12">
                            <div class="text-center" id="load-permission">
                                <img src="{{ url('image/load.gif') }}" height="200px">
                            </div>
                            <div id="tabel-permission">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.modal.role')
    @include('backend.modal.permission')
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2018 &copy; Laravel Content Management System  </footer>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    function getTabel(url,datas,type,load,result){
        $.ajax({
            url: url,
            data: datas,
            type: type,
            beforeSend: function(){
                $(load).css('display','block');
                $(result).css('display','none');
            },
            success: function(data){
                $(load).css('display','none');
                $(result).css('display','block');
                $(result).html(data);
            }
        });
    }
    $(document).ready(function() {
        getTabel('{{ route('role-get') }}',[],'GET','#load-role', '#tabel-role');
        getTabel('{{ route('permission-get') }}',[],'GET','#load-permission', '#tabel-permission');
        //on click jquery
        $('.formButtonRole').on('click',function(){
            var id = $(this).attr('data-id');
            $.ajax({
                url: '{{ route('role-form-create') }}',
                data: {
                    id : id
                },
                type: 'GET',
                beforeSend: function(){
                    $('.modal-title').html('Create Role');
                    $('.load-data').css('display','block');
                    $('.form-data').css('display','none');
                },
                success: function(data){
                    $('.load-data').css('display','none');
                    $('.form-data').css('display','block');
                    $('.form-data').html(data);
                }
            });
        });
        $('.formButtonPermission').on('click',function(){
            var id = $(this).attr('data-id');
            $.ajax({
                url: '{{ route('permission-form-create') }}',
                data: {
                    id : id
                },
                type: 'GET',
                beforeSend: function(){
                    $('.modal-title').html('Create Permission');
                    $('.load-permission').css('display','block');
                    $('.form-permission').css('display','none');
                },
                success: function(data){
                    $('.load-permission').css('display','none');
                    $('.form-permission').css('display','block');
                    $('.form-permission').html(data);
                }
            });
        });

        $('#delete-role').on('click',function(){
            $.ajax({
                url: '{{ route('role-delete') }}',
                data: {
                    id : dataid
                },
                type: 'GET',
                beforeSend: function(){
                    $('.load-delete').css('display','block');
                    $('.form-delete').css('display','none');
                },
                success: function(data){
                    $('.load-delete').css('display','none');
                    $('.form-delete').css('display','block');
                    $('#modalDeleteRole').modal('hide');
                    $('#alert-success').css('display','block');
                    getTabel('{{ route('role-get') }}',[],'GET','#load-role', '#tabel-role');
                    return true;
                }
            });
        });
        $('#delete-permission').on('click',function(){
            $.ajax({
                url: '{{ route('permission-delete') }}',
                data: {
                    id : dataid
                },
                type: 'GET',
                beforeSend: function(){
                    $('.load-permission').css('display','block');
                    $('.form-permission').css('display','none');
                },
                success: function(data){
                    $('.load-permission').css('display','none');
                    $('.form-permission').css('display','block');
                    $('#modalDeletePermission').modal('hide');
                    $('#alert-success').css('display','block');
                    getTabel('{{ route('permission-get') }}',[],'GET','#load-permission','#tabel-permission');
                }
            });
        });
        
    });
    $(document).ajaxSuccess(function() {
        //role
        $('#form-role').on('submit',function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'{{ route('role-create') }}',
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $('.load-data').css('display','block');
                    $('.form-data').css('display','none');
                },
                success:function(data){
                    if(data['data']['error'] == null){
                        $('.load-data').css('display','none');
                        $('.form-data').css('display','block');
                        $('#modalCreateRole').modal('hide');
                        $('#alert-success').css('display','block');
                        getTabel('{{ route('role-get') }}',[],'GET','#load-role', '#tabel-role');
                        return true;
                    }else{
                        $('.load-data').css('display','none');
                        $('.form-data').css('display','block');
                        $('#alert-error').css('display','block');
                        $('#message-error').html(data['data']['error']['message']);
                        
                        return true;
                    }

                }

            });
            return false;
        });
        $('#form-editrole').on('submit',function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'{{ route('role-edit') }}',
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $('.load-data').css('display','block');
                    $('.form-data').css('display','none');
                },
                success:function(data){
                    if(data['data']['error'] == null){
                        $('.load-data').css('display','none');
                        $('.form-data').css('display','block');
                        $('#modalCreateRole').modal('hide');
                        $('#alert-success').css('display','block');
                        getTabel('{{ route('role-get') }}',[],'GET','#load-role', '#tabel-role');
                        return true;
                    }else{
                        $('.load-data').css('display','none');
                        $('.form-data').css('display','block');
                        $('#alert-error').css('display','block');
                        $('#message-error').html(data['data']['error']['message']);
                        
                        return true;
                    }

                }

            });
            return false;
        });
        //permission
        $('#form-permission').on('submit',function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'{{ route('permission-create') }}',
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $('.load-permission').css('display','block');
                    $('.form-permission').css('display','none');
                },
                success:function(data){
                    if(data['data']['error'] == null){
                        $('.load-permission').css('display','none');
                        $('.form-permission').css('display','block');
                        $('#modalCreatePermission').modal('hide');
                        $('#alert-success').css('display','block');
                        getTabel('{{ route('permission-get') }}',[],'GET','#load-permission','#tabel-permission');
                        return true;
                    }else{
                        $('.load-permission').css('display','none');
                        $('.form-permission').css('display','block');
                        $('#alert-error').css('display','block');
                        $('#message-error').html(data['data']['error']['message']);
                        
                        return true;
                    }

                }

            });
            return false;
        });
        $('#form-editpermission').on('submit',function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'{{ route('role-edit') }}',
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $('.load-data').css('display','block');
                    $('.form-data').css('display','none');
                },
                success:function(data){
                    if(data['data']['error'] == null){
                        $('.load-data').css('display','none');
                        $('.form-data').css('display','block');
                        $('#modalCreateRole').modal('hide');
                        $('#alert-success').css('display','block');
                        getTabel('{{ route('role-get') }}',[],'GET','#load-role', '#tabel-role');
                        return true;
                    }else{
                        $('.load-data').css('display','none');
                        $('.form-data').css('display','block');
                        $('#alert-error').css('display','block');
                        $('#message-error').html(data['data']['error']['message']);
                        
                        return true;
                    }

                }

            });
            return false;
        });
    });
</script>
@endpush