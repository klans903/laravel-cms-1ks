<div class="table-responsive">
    <table class="table table-bordered">
    	<thead>
	        <tr>
	        	<th class="text-center">No.</th>
	        	<th class="text-center">Role</th>
	        	<th class="text-center">Permission</th>
	        	<th class="text-center">Action</th>
	        </tr>
	    </thead>
	    <tbody>
	    	@php
	    		$no = 0;
	    	@endphp
	    	@foreach($data as $key)
	    	@php
	    		$no++;
	    	@endphp
		    	<tr>
		    		<td class="text-center">{{ $no }}.</td>
		    		<td class="text-center">{{ $key->name }}</td>
		    		<td class="text-left">
		    			<ul class="list-icons">
		    				@foreach($key->permissions as $per)
		    					<li style="line-height: 15px;"><i class="fa fa-caret-right text-info" ></i> {{ $per->name }}</li>
		    				@endforeach
		    			</ul>
		    		</td>
		    		<td class="text-center">
		    			<a class="editRole" data-toggle="modal" data-target="#modalCreateRole" data-id={{ $key->id }}><button type="button" class="btn btn-warning btn-circle btn-sm m-r-5"><i class="ti-pencil-alt"></i></button></a>
                        <a class="deleteRole" data-toggle="modal" data-target="#modalDeleteRole" data-id={{ $key->id }}><button type="button" class="btn btn-danger  btn-circle btn-sm m-r-5"><i class="ti-trash"></i></button></a>
		    		</td>
		    	</tr>
	    	@endforeach
	    </tbody>
    </table>
</div>
<script type="text/javascript">
	$('.editRole').on('click',function (){
		var id = $(this).attr('data-id');
        $.ajax({
            url: '{{ route('role-form-edit') }}',
            data: {
                id : id
            },
            type: 'GET',
            beforeSend: function(){
                $('.modal-title').html('Edit Role');
                $('.load-data').css('display','block');
                $('.form-data').css('display','none');
            },
            success: function(data){
                $('.load-data').css('display','none');
                $('.form-data').css('display','block');
                $('.form-data').html(data);

            }
        });
	});
	$('.deleteRole').on('click',function (){
        dataid = $(this).attr('data-id');
	});
</script>